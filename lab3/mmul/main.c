// This program implements a vector addition using OpenCL

// System includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <time.h>

// OpenCL includes
#include <CL/cl.h>
#include "mmul_cl.h"

#define ni 1024
#define nk 1024
#define nj 1024

inline void checkErr(cl_int err, const char * name) {
   if (err != CL_SUCCESS) {
      fprintf(stderr, "ERROR: %s (%d)\n", name, err);
      exit(EXIT_FAILURE);
   }
}

// Serial matrix multiplication
void mmul(float *A, float *B, float *C)
{
   int i, j, k;

   memset(C, 0, sizeof(float) * ni * nj);

   for (i=0; i<ni; i++) {
      for (j=0; j<nj; j++) {
         for (k=0; k<nk; k++) {
            C[i * nj + j] += A[i * nj + k] * B[k * ni + j];
         }
      }
   }
}

int compute_diff(float *C, float *Cans){
   int cnt = 0;
   int i, j;
   float diff = 0.0;

   for (i=0; i<ni; i++) {
      for (j=0; j<nj; j++) {
         diff = fabs(C[i * nj + j] - Cans[i * nj + j]);
         // printf("%f %f\n", C[i * nj + j], Cans[i * nj + j]);
         if(diff > 1e-4){
            cnt++;
         }
      }
   }
   return cnt;
}

int main() {
   // This code executes on the OpenCL host

   // Host data
   float *A = NULL;  // Input matrix
   float *B = NULL;  // Input matrix
   float *C = NULL;  // Output matrix
   float *Cans = NULL;

   // Timing stuff
   struct	timeval ts, te, td;
   float tser, tpar;

   // Elements in each array
   const int elements = ni * nj;
   printf("Element size: %d\n", elements);

   // Compute the size of the data 
   size_t datasize = sizeof(float) * ni * nj;
   printf("Datasize %d\n", datasize);

   // Allocate space for input/output data
   A = (float*)malloc(datasize);
   B = (float*)malloc(datasize);
   C = (float*)malloc(datasize);
   Cans = (float *)malloc(datasize); 

   // Initialize the input data
   srand(3);
   for(int i = 0; i < ni; i++) {
      for(int j = 0; j < nj; j++) {
         A[i * nj + j] = ((float)rand()) / RAND_MAX;
         B[i * nj + j] = ((float)rand()) / RAND_MAX;
      }
   }

   printf("Finished loading data\n");
   // Use this to check the output of each API call
   cl_int status;  

   // Retrieve the number of platforms
   cl_uint numPlatforms = 0;
   status = clGetPlatformIDs(0, NULL, &numPlatforms);
   checkErr(status, "Retrieve the number of platforms");

   // Allocate enough space for each platform
   cl_platform_id *platforms = NULL;
   platforms = (cl_platform_id*)malloc(
         numPlatforms * sizeof(cl_platform_id));

   // Fill in the platforms
   status = clGetPlatformIDs(numPlatforms, platforms, NULL); checkErr(status, "Fill in the platforms");

   // Find CPU
   int platform_index = -1;
   for (int i = 0; i < numPlatforms; i++)
   {
      char vendor[128];
      clGetPlatformInfo (platforms[i], CL_PLATFORM_VENDOR, sizeof(vendor), vendor, NULL);
      char vendorF[7];
      memcpy((void*)vendorF, (void*)vendor, 6);
      vendorF[6] = '\0';
      fprintf(stderr, "%s\n", vendorF);
      if (strcmp(vendorF, "Intel(") == 0)
      {
         platform_index = i;
         break;
      }
   }
   if (platform_index == -1){
      printf("CPU platform not found!\n");
      exit(1);
   }

   // Retrieve the number of devices
   cl_uint numDevices = 0;
   status = clGetDeviceIDs(platforms[platform_index], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices);
   checkErr(status, "Retrieve the number of devices");
   printf("#devices: %d, status %d\n", numDevices, status);

   // Allocate enough space for each device
   cl_device_id *devices;
   devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));

   // Fill in the devices 
   status = clGetDeviceIDs(platforms[platform_index], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL);
   checkErr(status, "Fill in the devices");

   // Create a context and associate it with the devices
   cl_context context;
   context = clCreateContext(NULL, numDevices, devices, NULL, 
         NULL, &status);

   // Create a command queue and associate it with the device 
   cl_command_queue cmdQueue;
   cmdQueue = clCreateCommandQueue(context, devices[0], 0, 
         &status);

   // Create a buffer object that will contain the data 
   // from the host array A
   cl_mem bufA;
   bufA = clCreateBuffer(context, CL_MEM_READ_ONLY, datasize,                       
         NULL, &status);

   // Create a buffer object that will contain the data 
   // from the host array B
   cl_mem bufB;
   bufB = clCreateBuffer(context, CL_MEM_READ_ONLY, datasize,                        
         NULL, &status);

   // Create a buffer object that will hold the output data
   cl_mem bufC;
   bufC = clCreateBuffer(context, CL_MEM_READ_WRITE, datasize,
         NULL, &status); 

   // Write input array A to the device buffer bufferA
   status = clEnqueueWriteBuffer(cmdQueue, bufA, CL_TRUE, 
         0, datasize, A, 0, NULL, NULL);
   checkErr(status, "Write buffer A");

   // Write input array B to the device buffer bufferB
   status = clEnqueueWriteBuffer(cmdQueue, bufB, CL_TRUE, 
         0, datasize, B, 0, NULL, NULL);
   checkErr(status, "Write buffer B");

   // Create a program with source code
   cl_program program = clCreateProgramWithSource(context, 1, 
         (const char**)&mmul_cl, NULL, &status);

   // Build (compile) the program for the device
   status = clBuildProgram(program, numDevices, devices, 
         NULL, NULL, NULL);
   if (status == CL_BUILD_PROGRAM_FAILURE) {
      size_t log_size;
      clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, 0,
            NULL, &log_size);
      char *log = (char*)malloc(log_size);
      clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG,
            log_size, log, NULL);
      fprintf(stderr, "%s\n", log);
      exit(1);
   }
   checkErr(status, "Build program");

   // Create the vector addition kernel
   cl_kernel kernel;
   kernel = clCreateKernel(program, "mmul", &status);

   // Associate the input and output buffers with the kernel 
   status = clSetKernelArg(kernel, 0, sizeof(cl_mem), &bufA);
   checkErr(status, "Set Arg 0");
   status = clSetKernelArg(kernel, 1, sizeof(cl_mem), &bufB);
   checkErr(status, "Set Arg 1");
   status = clSetKernelArg(kernel, 2, sizeof(cl_mem), &bufC);
   checkErr(status, "Set Arg 2");

   // Define an index space (global work size) of work 
   // items for execution. A workgroup size (local work size) 
   // is not required, but can be used.
   size_t globalWorkSize[2] = {4, 4};   

   // Execute the kernel

   gettimeofday(&ts, NULL);
   status = clEnqueueNDRangeKernel(cmdQueue, kernel, 2, NULL, 
         globalWorkSize, NULL, 0, NULL, NULL);
   checkErr(status, "Execute kernel");

   // Read the device output buffer to the host output array
   clEnqueueReadBuffer(cmdQueue, bufC, CL_TRUE, 0, 
         datasize, C, 0, NULL, NULL);
   checkErr(status, "Dequeue C");

   gettimeofday(&te, NULL);
   timersub(&ts, &te, &td);
   tpar = fabs(td.tv_sec+(float)td.tv_usec/1000000.0);

   // Verify the output
   mmul(A, B, Cans);

   int diff  = compute_diff(C, Cans);

   printf("Time : %.2f sec \n", tpar);
   printf("Performance : %.2f GFlop/s\n", 2.0*ni*nj*nk/tpar/1000000000);
   printf("Result Diff : %d\n\n", diff );

   // Free OpenCL resources
   clReleaseKernel(kernel);
   clReleaseProgram(program);
   clReleaseCommandQueue(cmdQueue);
   clReleaseMemObject(bufA);
   clReleaseMemObject(bufB);
   clReleaseMemObject(bufC);
   clReleaseContext(context);

   // Free host resources
   free(A);
   free(B);
   free(C);
   free(Cans);
   free(platforms);
   free(devices);

   return 0;
}
