#define ni 1024
#define nj 1024
#define nk 1024

#define PBLOCK 256
#define BLOCK 32

   __kernel                                            
void mmul(
      __global float *A,                        
      __global float *B,                        
      __global float *C)                        
{                                                   
   int r = get_global_id(0); 
   int c = get_global_id(1);

   printf("CPU %d %d\n", r, c);

   float C_buf[BLOCK][BLOCK];
   float A_buf[BLOCK][BLOCK];
   float B_buf[BLOCK][BLOCK];

   for(int ii = r * PBLOCK; ii < (r + 1) * PBLOCK; ii += BLOCK){
      for(int jj = c * PBLOCK; jj < (c + 1) * PBLOCK; jj += BLOCK){

         // Set C_buf to zero
         for(int i = 0; i < BLOCK; i++){
            for(int j = 0;j < BLOCK; j++){
               C_buf[i][j] = 0;
            }
         }

         for(int kk = 0; kk < nk; kk += BLOCK){
            // C_buf[ii][jj] += A[ii][kk] * B[kk][jj]
            
            // Load buffer A
            for(int i = 0; i < BLOCK; i++){
               for(int j = 0; j < BLOCK; j++){
                  A_buf[i][j] = A[ (ii + i) * nj + kk + j];
                  B_buf[i][j] = B[ (kk + i) * nj + jj + j];
               }
            }

            for(int i = 0; i < BLOCK; i++){
               for(int k = 0; k < BLOCK; k++){ 
                  for(int j = 0; j < BLOCK; j++){
                     C_buf[i][j] += A_buf[i][k] * B_buf[k][j];
                  }
               }
            }
         }

         // Writeback
         for(int i = 0; i < BLOCK; i++){
            for(int j = 0; j < BLOCK; j++){
               C[ (ii + i) * nj + jj + j ] = C_buf[i][j];
            }
         }
      }
   }
}                                                   
