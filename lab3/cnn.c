#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>

// OpenCL includes
#include <CL/cl.h>
#include "kernel_cl.h"
#include "cnn.h"

// Sequential CNN implementation
void conv(float Cout[NUM][OUTIMROW][OUTIMROW], float Cin[NUM][INIMROW][INIMROW],
          float weight[NUM][NUM][KERNEL][KERNEL], float bias[NUM]) {
    static float C[NUM][IMROW][IMROW];

    for (int i = 0; i < NUM; i++) {
        for (int h = 0; h < IMROW; h++) {
            for (int w = 0; w < IMROW; w++)
                C[i][h][w] = bias[i];
        }
    }

// Convolution
    for (int i = 0; i < NUM; i++) {
        for (int j = 0; j < NUM; j++) {
            for (int h = 0; h < IMROW; h++) {
                for (int w = 0; w < IMROW; w++) {
                    for (int p = 0; p < KERNEL; p++) {
                        for (int q = 0; q < KERNEL; q++)
                            C[i][h][w] += weight[i][j][p][q] * Cin[j][h + p][w + q];
                    }
                }
            }
        }
    }

// ReLU
    for (int i = 0; i < NUM; i++) {
        for (int h = 0; h < IMROW; h++) {
            for (int w = 0; w < IMROW; w++) {
                C[i][h][w] = fmax(0, C[i][h][w]);
            }
        }
    }

// Max pooling
    for (int i = 0; i < NUM; i++) {
        for (int h = 0; h < OUTIMROW; h++) {
            for (int w = 0; w < OUTIMROW; w++) {
                float local_max = C[i][2 * h][2 * w];
                local_max = fmax(local_max, C[i][2 * h + 1][2 * w]);
                local_max = fmax(local_max, C[i][2 * h + 1][2 * w + 1]);
                local_max = fmax(local_max, C[i][2 * h][2 * w + 1]);
                Cout[i][h][w] = local_max;
            }
        }
    }
}

inline void checkErr(cl_int err, const char * name) {
    if (err != CL_SUCCESS) {
        fprintf(stderr, "ERROR: %s (%d)\n", name, err);
        exit(EXIT_FAILURE);
    }
}

int main() {
    static float Cout[NUM][OUTIMROW][OUTIMROW];
    static float Cin[NUM][INIMROW][INIMROW];
    static float weight[NUM][NUM][KERNEL][KERNEL];
    static float bias[NUM];

    LoadData(Cin, weight, bias);

    // OpenCL host program
    fprintf(stderr, "Start cnn computation\n");
    struct timeval t1, t2;
    gettimeofday(&t1, NULL);

    // --- Please add your code below ---
//    conv(Cout, Cin, weight, bias);

    // Use this to check the output of each API call
    cl_int status;

    // Retrieve the number of platforms
    cl_uint numPlatforms = 0;
    status = clGetPlatformIDs(0, NULL, &numPlatforms);
    checkErr(status, "Retrieve the number of platforms");

    // Allocate enough space for each platform
    cl_platform_id *platforms = NULL;
    platforms = (cl_platform_id*)malloc(numPlatforms * sizeof(cl_platform_id));

    // Fill in the platforms
    status = clGetPlatformIDs(numPlatforms, platforms, NULL);
    checkErr(status, "Fill in the platforms");

    // Find CPU
    int platform_index = -1;
    for (int i = 0; i < numPlatforms; i++) {
        char vendor[128];
        clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, sizeof(vendor), vendor, NULL);
        char vendorF[7];
        memcpy((void*)vendorF, (void*)vendor, 6);
        vendorF[6] = '\0';
        fprintf(stderr, "%s\n", vendorF);
        if (strcmp(vendorF, "NVIDIA") == 0) {
            platform_index = i;
            break;
        }
    }
    if (platform_index == -1){
        printf("CPU platform not found!\n");
        exit(1);
    }

    // Retrieve the number of devices
    cl_uint numDevices = 0;
    status = clGetDeviceIDs(platforms[platform_index], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices);
    checkErr(status, "Retrieve the number of devices");
    printf("#devices: %d, status %d\n", numDevices, status);

    // Allocate enough space for each device
    cl_device_id *devices;
    devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));

    // Fill in the devices
    status = clGetDeviceIDs(platforms[platform_index], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL);
    checkErr(status, "Fill in the devices");

    // Create a context and associate it with the devices
    cl_context context;
    context = clCreateContext(NULL, numDevices, devices, NULL, NULL, &status);

    // Create a command queue and associate it with the device
    cl_command_queue cmdQueue;
    cmdQueue = clCreateCommandQueue(context, devices[0], 0, &status);

    // Create a buffer object that will contain the data
    cl_mem buf_Cout, buf_Cin, buf_weight, buf_bias;
    int Cout_size = NUM * OUTIMROW * OUTIMROW * sizeof(float);
    int Cin_size = NUM * INIMROW * INIMROW * sizeof(float);
    int weight_size = NUM * NUM * KERNEL * KERNEL * sizeof(float);
    int bias_size = NUM * sizeof(float);
    buf_Cout = clCreateBuffer(context, CL_MEM_READ_WRITE, NUM * OUTIMROW * OUTIMROW * sizeof(float), NULL, &status);
    buf_Cin = clCreateBuffer(context, CL_MEM_READ_ONLY, NUM * INIMROW * INIMROW * sizeof(float), NULL, &status);
    buf_weight = clCreateBuffer(context, CL_MEM_READ_ONLY, NUM * NUM * KERNEL * KERNEL * sizeof(float), NULL, &status);
    buf_bias = clCreateBuffer(context, CL_MEM_READ_ONLY, NUM * sizeof(float), NULL, &status);

    // Write input to the device buffer buf_Cin
    status = clEnqueueWriteBuffer(cmdQueue, buf_Cin, CL_TRUE, 0, Cin_size, Cin, 0, NULL, NULL);
    checkErr(status, "Write buffer Cin");

    // Write input to the device buffer buf_weight
    status = clEnqueueWriteBuffer(cmdQueue, buf_weight, CL_TRUE, 0, weight_size, weight, 0, NULL, NULL);
    checkErr(status, "Write buffer weight");

    // Write input to the device buffer buf_bias
    status = clEnqueueWriteBuffer(cmdQueue, buf_bias, CL_TRUE, 0, bias_size, bias, 0, NULL, NULL);
    checkErr(status, "Write buffer bias");

    // Create a program with source code
    cl_program program = clCreateProgramWithSource(context, 1, (const char**)&kernel_cl, NULL, &status);

    // Build (compile) the program for the device
    status = clBuildProgram(program, numDevices, devices, NULL, NULL, NULL);
    if (status == CL_BUILD_PROGRAM_FAILURE) {
        size_t log_size;
        clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
        char *log = (char*)malloc(log_size);
        clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, log_size, log, NULL);
        fprintf(stderr, "%s\n", log);
        exit(1);
    }
    checkErr(status, "Build program");

    // Create the conv kernel
    cl_kernel kernel;
    kernel = clCreateKernel(program, "conv", &status);

    // Associate the input and output buffers with the kernel
    status = clSetKernelArg(kernel, 0, sizeof(cl_mem), &buf_Cout);
    checkErr(status, "Set Arg 0");
    status = clSetKernelArg(kernel, 1, sizeof(cl_mem), &buf_Cin);
    checkErr(status, "Set Arg 1");
    status = clSetKernelArg(kernel, 2, sizeof(cl_mem), &buf_weight);
    checkErr(status, "Set Arg 2");
    status = clSetKernelArg(kernel, 3, sizeof(cl_mem), &buf_bias);
    checkErr(status, "Set Arg 3");

    // Define an index space (global work size) of work
    // items for execution. A workgroup size (local work size)
    // is not required, but can be used.
    size_t globalWorkSize[1] = {256};

    // Execute the kernel
    status = clEnqueueNDRangeKernel(cmdQueue, kernel, 1, NULL, globalWorkSize, NULL, 0, NULL, NULL);
    checkErr(status, "Execute kernel");

    // Read the device output buffer to the host output array
    clEnqueueReadBuffer(cmdQueue, buf_Cout, CL_TRUE, 0, Cout_size, Cout, 0, NULL, NULL);
    checkErr(status, "Dequeue C");

    gettimeofday(&t2, NULL);
    float elapsed_time = (t2.tv_sec - t1.tv_sec) + (t2.tv_usec - t1.tv_usec) / 1e6;
    fprintf(stderr, "time(s): %f\n", elapsed_time);
    fprintf(stderr, "GOPs: %f\n", (float)NUM * NUM * IMROW * IMROW * KERNEL * KERNEL * 2 / elapsed_time / 1e9);

    int error = Verify(Cout);
    if(error != 0)
        fprintf(stderr, "error ocurrs %d\n", error);
    else
        fprintf(stderr, "all right!\n");

    return 0;
}
