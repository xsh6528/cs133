#define NUM 256
#define INIMROW 228
#define IMROW 224
#define OUTIMROW 112
#define KERNEL 5

#define BSIZE 56

__kernel
void conv(__global float Cout[NUM][OUTIMROW][OUTIMROW], __global float Cin[NUM][INIMROW][INIMROW],
          __global float weight[NUM][NUM][KERNEL][KERNEL], __global float bias[NUM]) {

    int global_id = get_global_id(0);

    float C[IMROW][IMROW];
    float buf_Cin[BSIZE + KERNEL - 1][BSIZE + KERNEL - 1];
    float buf_weight[KERNEL][KERNEL];

        int i = global_id;

        // Bias
        for (int h = 0; h < IMROW; ++h) {
            for (int w = 0; w < IMROW; ++w) {
                C[h][w] = bias[i];
            }
        }

        // Convolution
        for(int j = 0; j < NUM; ++j) {

            for (int p = 0; p < KERNEL; ++p) {
                for (int q = 0; q < KERNEL; ++q) {
                    buf_weight[p][q] = weight[i][j][p][q];
                }
            }

            for (int hh = 0; hh < IMROW; hh += BSIZE) {
                for (int ww = 0; ww < IMROW; ww += BSIZE) {

                    for (int h = 0; h < BSIZE + KERNEL - 1; h++) {
                        for (int w = 0; w < BSIZE + KERNEL - 1; w++) {
                            buf_Cin[h][w] = Cin[j][h + hh][w + ww];
                        }
                    }

                    for(int h = hh; h < hh + BSIZE; ++h) {
                        for(int w = ww; w < ww + BSIZE; ++w) {
                            for(int p = 0; p < KERNEL; ++p) {
                                for(int q = 0; q < KERNEL; ++q){
                                    C[h][w] += buf_weight[p][q] * buf_Cin[h + p - hh][w + q - ww];
                                }
                            }
                        }
                    }

                }
            }
        }

        // ReLU
        for (int h = 0; h < IMROW; ++h) {
            for (int w = 0; w < IMROW; ++w) {
                C[h][w] = fmax((float)0, C[h][w]);
            }
        }

        // Max pooling
        for (int h = 0; h < OUTIMROW; ++h) {
            for (int w = 0; w < OUTIMROW; ++w) {
                float local_max = C[2 * h][2 * w];
                local_max = fmax(local_max, C[2 * h + 1][2 * w]);
                local_max = fmax(local_max, C[2 * h + 1][2 * w + 1]);
                local_max = fmax(local_max, C[2 * h][2 * w + 1]);
                Cout[i][h][w] = local_max;
            }
        }

}
