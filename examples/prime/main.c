#include <time.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <omp.h>

#define NUM 10000
#define ENUM 1000000

int is_prime_seq(long n) {
    int count = 0;

    for (int i = 2; i < n; ++i) {
        if (n % i == 0) {
//			return 1;
            ++count;
        }
    }

//	return 0;
    return count > 0;
}

int count_primes_seq(long N) {
    int count = 0;
    for (int n = 2; n < N; ++n) {
        count += is_prime_seq(n);
    }

    return count;
}


int count_primes_par(long N) {
    int count = 0;

#pragma omp parallel for firstprivate(N) reduction(+:count)
    for (int n = 2; n < N; ++n) {
        count += is_prime_seq(n);
    }

    return count;
}

int eras_seq(long n) {
    int *vec = (int *) malloc(sizeof(int) * (n + 1));
    int count = 0;
    int sroot = sqrt(n);

    for (int i = 2; i < n; ++i) {
        vec[i] = 1;
    }

    for (int i = 2; i <= sroot; ++i) {
        if (vec[i]) {
            for (int j = i + i; j <= n; j += i) {
                vec[i] = 0;
            }
        }
    }

    for (int i = 2; i <= n; ++i) {
        count += vec[i];
    }

    return count;
}

int eras_par(long n) {
    int *vec = (int *) malloc(sizeof(int) * (n + 1));
    int count = 0;
    int sroot = sqrt(n);
    int j;

    for (int i = 2; i < n; ++i) {
        vec[i] = 1;
    }

#pragma omp parallel for private(j)
    for (int i = 2; i < sroot; ++i) {
        if (vec[i]) {
            for (j = i + i; j <= n; j += i) {
                vec[i] = 0;
            }
        }
    }

#pragma omp parallel for reduction(+:count)
    for (int i = 2; i <= n; ++i) {
        count += vec[i];
    }

    return count;
}

/*
 * Implement a blocked version of the erasthosthenis sieve.
 * The major overhead in the version above, is the use of the shared vector.
 * If each thread allocates its own memory, we can get a much bigger speedup!
 */
int eras_par_blocked(int n) {
    // TODO

    return 0;
}

int main() {
    double start, stop;

    printf("Setting n to %d\n", NUM);
    start = omp_get_wtime();
    count_primes_seq(NUM);
    stop = omp_get_wtime();
    printf("Naive serial prime count took [%f] seconds\n", stop - start);

    omp_set_num_threads(32);

    start = omp_get_wtime();
    count_primes_par(NUM);
    stop = omp_get_wtime();
    printf("Naive parallel prime count took [%f] seconds\n", stop - start);
    printf("\n\n");

    printf("Changing n to %d\n", ENUM);
    start = omp_get_wtime();
    eras_seq(ENUM);
    stop = omp_get_wtime();
    printf("Erasthosthenis sieve sequential primality check took [%f] seconds\n", stop - start);

    start = omp_get_wtime();
    eras_par(ENUM);
    stop = omp_get_wtime();
    printf("Parallel erasthosthenis sieve sequential primality check took [%f] seconds\n", stop - start);

    start = omp_get_wtime();
    eras_par_blocked(ENUM);
    stop = omp_get_wtime();
    printf("Blocked parallel erasthosthenis sieve sequential primality check took [%f] seconds\n", stop - start);
}
