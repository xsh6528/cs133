#include "const.h"
#include <omp.h>

void mmul1(float A[ni][nk], float B[nk][nj], float C[ni][nj]) {
    int i, k, j;

    #pragma omp parallel for private(j)
    for (i = 0; i < ni; ++i) {
        for (j = 0; j < nj; ++j) {
            C[i][j] = 0;
        }
    }

    #pragma omp parallel for private(i, j, k)  // parallel directive
    for (i = 0; i < ni; ++i) {
        for (k = 0; k < nk; ++k) {  // loop interchange

            float temp_A_ik = A[i][k];  // hold variable in register
            for (j = 0; j < nj; ++j) {
                C[i][j] += temp_A_ik * B[k][j];
            }

        }
    }

    // TODO why k cannot be outer most
}
