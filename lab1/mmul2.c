#include "const.h"
#include <omp.h>
#include <string.h>

#define bsize 64

void mmul2(float A[ni][nk], float B[nk][nj], float C[ni][nj]) {
    int i, j, k, ii, jj;

    // TODO why bsize 64
    #pragma omp parallel for private(i, j, k, ii, jj)  // parallel directive
    for (ii = 0; ii < ni; ii += bsize) {
        for (jj = 0; jj < nj; jj += bsize) {  // loop tiling

            float temp_C_tile[bsize][bsize] = {0};  // local buffered C tile

            for (k = 0; k < nk; k++) {
                for (i = ii; i < ii + bsize; ++i) {  // loop interchange

                    float temp_A_ik = A[i][k];  // hold variable in register
                    for (j = jj; j < jj + bsize; ++j) {
                        temp_C_tile[i - ii][j - jj] += temp_A_ik * B[k][j];
                    }

                }
            }

            for (i = ii; i < ii + bsize; ++i) {  // copy local C tile to shared C
                memcpy(&C[i][jj], &temp_C_tile[i - ii][0], bsize * sizeof(float));
            }
        }
    }

    // TODO why k must be outer loop

//    for (j = jj; j < jj + bsize; ++j) {
//        C[i][j] = temp_C[i - ii][j - jj];
//    }
}

// baseline 4
void mmul4(float A[ni][nk], float B[nk][nj], float C[ni][nj]) {
    int i, k, j;

#pragma omp parallel for private(j)
    for (i = 0; i < ni; ++i) {
        for (j = 0; j < nj; ++j) {
            C[i][j] = 0;
        }
    }

#pragma omp parallel for private(i, j, k)  // parallel directive
    for (i = 0; i < ni; ++i) {
        for (j = 0; j < nj; ++j) {
            for (k = 0; k < nk; ++k) {
                C[i][j] += A[i][k] * B[k][j];
            }
        }
    }
}

// interchange 5
void mmul5(float A[ni][nk], float B[nk][nj], float C[ni][nj]) {
    int i, k, j;

#pragma omp parallel for private(j)
    for (i = 0; i < ni; ++i) {
        for (j = 0; j < nj; ++j) {
            C[i][j] = 0;
        }
    }

#pragma omp parallel for private(i, j, k)  // parallel directive
    for (i = 0; i < ni; ++i) {
        for (k = 0; k < nk; ++k) {  // loop interchange
            for (j = 0; j < nj; ++j) {
                C[i][j] += A[i][k] * B[k][j];
            }

        }
    }
}

// interchange + register 6
void mmul6(float A[ni][nk], float B[nk][nj], float C[ni][nj]) {
    int i, k, j;

#pragma omp parallel for private(j)
    for (i = 0; i < ni; ++i) {
        for (j = 0; j < nj; ++j) {
            C[i][j] = 0;
        }
    }

#pragma omp parallel for private(i, j, k)  // parallel directive
    for (i = 0; i < ni; ++i) {
        for (k = 0; k < nk; ++k) {  // loop interchange
            float temp_A_ik = A[i][k];  // hold variable in register
            for (j = 0; j < nj; ++j) {
                C[i][j] += temp_A_ik * B[k][j];
            }
        }
    }
}

// tiling 7
void mmul7(float A[ni][nk], float B[nk][nj], float C[ni][nj]) {
    int i, j, k, ii, jj;

#pragma omp parallel for private(j)
    for (i = 0; i < ni; ++i) {
        for (j = 0; j < nj; ++j) {
            C[i][j] = 0;
        }
    }

#pragma omp parallel for private(i, j, k, ii, jj)  // parallel directive
    for (ii = 0; ii < ni; ii += bsize) {
        for (jj = 0; jj < nj; jj += bsize) {  // loop tiling

            for (i = ii; i < ii + bsize; ++i) {
                for (j = jj; j < jj + bsize; ++j) {
                    for (k = 0; k < nk; k++) {
                        C[i][j] += A[i][k] * B[k][j];
                    }
                }
            }

        }
    }
}

// tiling + buffer 8
void mmul8(float A[ni][nk], float B[nk][nj], float C[ni][nj]) {
    int i, j, k, ii, jj;

#pragma omp parallel for private(i, j, k, ii, jj)  // parallel directive
    for (ii = 0; ii < ni; ii += bsize) {
        for (jj = 0; jj < nj; jj += bsize) {  // loop tiling

            float temp_C_tile[bsize][bsize] = {0};  // local buffered C tile

            for (i = ii; i < ii + bsize; ++i) {
                for (j = jj; j < jj + bsize; ++j) {
                    for (k = 0; k < nk; k++) {
                        temp_C_tile[i - ii][j - jj] += A[i][k] * B[k][j];
                    }
                }
            }

            for (i = ii; i < ii + bsize; ++i) {  // copy local C tile to shared C
                memcpy(&C[i][jj], &temp_C_tile[i - ii][0], bsize * sizeof(float));
            }
        }
    }
}
