//
// Created by Shihao Xu on 5/22/17.
//

#define NUM 256
#define INIMROW 228
#define IMROW 224
#define OUTIMROW 112
#define KERNEL 5

#define BSIZE 16
#define TILE 4
#define FNUM 4

__kernel
void conv(__global float Cout[NUM][OUTIMROW][OUTIMROW],
          __global float Cin[NUM][INIMROW][INIMROW],
          __global float weight[NUM][NUM][KERNEL][KERNEL],
          __global float bias[NUM]) {

//    size_t globalWorkSize[3] = {256, 224, 224};
//    size_t localWorkSize[3] = {4, 16, 16};

    int gi = get_global_id(0);
    int gx = get_global_id(1);
    int gy = get_global_id(2);

    int li = get_local_id(0);
    int lx = get_local_id(1);
    int ly = get_local_id(2);

    float C_private = bias[gi];
    for (int jj = 0; jj < NUM; jj += TILE) {
        // load Cin
        __local float Cin_local[TILE][BSIZE + KERNEL - 1][BSIZE + KERNEL - 1];
        if (li == 0) {
            for (int j = 0; j < TILE; j++) {
                for (int p = lx; p < BSIZE + KERNEL - 1; p += BSIZE) {
                    for (int q = ly; q < BSIZE + KERNEL - 1; q += BSIZE) {
                        Cin_local[j][p][q] = Cin[jj + j][gx + p - lx][gy + q - ly];
                    }
                }
                if (lx == BSIZE - 1 && ly == BSIZE - 1) {
                    for (int p = 1; p < KERNEL; p++) {
                        for (int q = 1; q < KERNEL; q++) {
                            Cin_local[j][lx + p][ly + q] = Cin[jj + j][gx + p][gy + q];
                        }
                    }
                }
            }
        }

        // load weight
        float weight_local[TILE][KERNEL][KERNEL];
        for (int j = 0; j < TILE; j++) {
            for (int p = 0; p < KERNEL; p++) {
                for (int q = 0; q < KERNEL; q++) {
                    weight_local[j][p][q] = weight[gi][j][p][q];
                }
            }
        }

        barrier(CLK_LOCAL_MEM_FENCE);

        // Conv
        for (int j = 0; j < TILE; j++) {
            for (int p = 0; p < KERNEL; p++) {
                for (int q = 0; q < KERNEL; q++) {
                    C_private += weight_local[j][p][q] * Cin_local[j][lx + p][ly + q];
                }
            }
        }

        barrier(CLK_LOCAL_MEM_FENCE);
    }

    // ReLU
    C_private = fmax((float)0, C_private);

    __local float C_local[FNUM][IMROW][IMROW];
    C_local[li][lx][ly] = C_private;

    // Pooling
    barrier(CLK_LOCAL_MEM_FENCE);

    if (lx % 2 == 0 && ly % 2 == 0) {
        float max_private = C_private;

//        max_private = fmax(max_private, C_local[li][lx][ly + 1]);
//        max_private = fmax(max_private, C_local[li][lx + 1][ly]);
//        max_private = fmax(max_private, C_local[li][lx + 1][ly + 1]);

        Cout[gi][gx / 2][gy / 2] = max_private;
    }

}
