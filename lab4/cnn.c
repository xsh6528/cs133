#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>
#include "cnn.h"
#include <CL/cl.h>
#include "kernel_cl.h"
#include <string.h>

// Sequential CNN implementation
void conv(float Cout[NUM][OUTIMROW][OUTIMROW], float Cin[NUM][INIMROW][INIMROW],
          float weight[NUM][NUM][KERNEL][KERNEL], float bias[NUM]) {

	static float C[NUM][IMROW][IMROW];

	for(int i = 0; i < NUM; i++) {
		for(int h = 0; h < IMROW; h++) {
			for(int w = 0; w < IMROW; w++)
				C[i][h][w] = bias[i];
		}
	}

// Convolution
	for(int i = 0; i < NUM; i++) {
		for(int j = 0; j < NUM; j++) {
			for(int h = 0; h < IMROW; h++) {
				for(int w = 0; w < IMROW; w++) {
					for(int p = 0; p < KERNEL; p++) {
						for(int q = 0; q < KERNEL; q++)
							C[i][h][w] += weight[i][j][p][q] * Cin[j][h + p][w + q];
					}
				}
			}
		}
	}

// ReLU
	for (int i = 0; i < NUM; i++) {
		for (int h = 0; h < IMROW; h++) {
			for (int w = 0; w < IMROW; w++) {
				C[i][h][w] = fmax(0, C[i][h][w]);
			}	
		}
	}

// Max pooling
	for (int i = 0; i < NUM; i++) {
		for (int h = 0; h < OUTIMROW; h++) {
			for (int w = 0; w < OUTIMROW; w++) {
				float local_max = C[i][2 * h][2 * w];
				local_max = fmax(local_max, C[i][2 * h + 1][2 * w]);
				local_max = fmax(local_max, C[i][2 * h + 1][2 * w + 1]);
				local_max = fmax(local_max, C[i][2 * h][2 * w + 1]);
				Cout[i][h][w] = local_max;
			}
		}
	}
}

inline void checkErr(cl_int err, const char * name) {
    if (err != CL_SUCCESS) {
        fprintf(stderr, "ERROR: %s (%d)\n", name, err);
        exit(EXIT_FAILURE);
    }
}

void opencl_gpu(
   float Cout[NUM][OUTIMROW][OUTIMROW],
   float Cin[NUM][INIMROW][INIMROW],
   float weight[NUM][NUM][KERNEL][KERNEL],
   float bias[NUM]) {
    // please add your OpenCL setup code below

    // Use this to check the output of each API call
    cl_int status;

    // Retrieve the number of platforms
    cl_uint numPlatforms = 0;
    status = clGetPlatformIDs(0, NULL, &numPlatforms);
    checkErr(status, "Retrieve the number of platforms");

    cl_platform_id *platforms = NULL;
    platforms = (cl_platform_id *) malloc(
            numPlatforms * sizeof(cl_platform_id));

    // Fill in the platforms
    status = clGetPlatformIDs(numPlatforms, platforms, NULL);
    checkErr(status, "Fill in the platforms");

    int platform_index = -1;

    for (int i = 0; i < numPlatforms; i++) {
        char vendor[128];
        clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, sizeof(vendor), vendor, NULL);
        char vendorF[7];
        memcpy((void *) vendorF, (void *) vendor, 6);
        vendorF[6] = '\0';
        fprintf(stderr, "%s\n", vendorF);
        if (strcmp(vendorF, "NVIDIA") == 0) {
            platform_index = i;
            break;
        }
    }
    if (platform_index == -1) {
        printf("GPU platform not found!\n");
        exit(1);
    }

    cl_uint numDevices = 0;
    status = clGetDeviceIDs(platforms[platform_index], CL_DEVICE_TYPE_ALL, 0,
                            NULL, &numDevices);
    checkErr(status, "Retrieve the number of devices");
    printf("#devices: %d, status %d\n", numDevices, status);

    // Allocate enough space for each device
    cl_device_id *devices;
    devices = (cl_device_id *) malloc(
            numDevices * sizeof(cl_device_id));

    // Fill in the devices
    status = clGetDeviceIDs(platforms[platform_index], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL);
    checkErr(status, "Fill in the devices");

    // Create a context and associate it with the devices
    cl_context context;
    context = clCreateContext(NULL, numDevices, devices, NULL, NULL, &status);

    // Create a command queue and associate it with the device
    cl_command_queue cmdQueue;
    cmdQueue = clCreateCommandQueue(context, devices[0], 0, &status);
    checkErr(status, "Create command queue");

    int Cout_size = sizeof(float) * NUM * OUTIMROW * OUTIMROW;
    int Cin_size = sizeof(float) * NUM * INIMROW * INIMROW;
    int weight_size = sizeof(float) * NUM * NUM * KERNEL * KERNEL;
    int bias_size = sizeof(float) * NUM;

    // Create a buffer object that will contain the data
    // from the host array A
    cl_mem buf_Cin;
    buf_Cin = clCreateBuffer(context, CL_MEM_READ_ONLY, Cin_size,
                             NULL, &status);

    // Create a buffer object that will contain the data
    // from the host array B
    cl_mem buf_weight;
    buf_weight = clCreateBuffer(context, CL_MEM_READ_ONLY, weight_size,
                                NULL, &status);

    cl_mem buf_bias;
    buf_bias = clCreateBuffer(context, CL_MEM_READ_ONLY, bias_size,
                              NULL, &status);

    // Create a buffer object that will hold the output data
    cl_mem buf_Cout;
    buf_Cout = clCreateBuffer(context, CL_MEM_READ_WRITE, Cout_size,
                              NULL, &status);

    status = clEnqueueWriteBuffer(cmdQueue, buf_Cin, CL_TRUE,
                                  0, Cin_size, Cin, 0, NULL, NULL);
    checkErr(status, "Write buffer Cin");

    status = clEnqueueWriteBuffer(cmdQueue, buf_weight, CL_TRUE,
                                  0, weight_size, weight, 0, NULL, NULL);
    checkErr(status, "Write buffer weight");

    status = clEnqueueWriteBuffer(cmdQueue, buf_bias, CL_TRUE,
                                  0, bias_size, bias, 0, NULL, NULL);
    checkErr(status, "Write buffer bias");

    // Create a program with source code
    cl_program program = clCreateProgramWithSource(context, 1,
                                                   (const char **) &kernel_cl, NULL, &status);

    // Build (compile) the program for the device
    status = clBuildProgram(program, numDevices, devices,
                            NULL, NULL, NULL);

    if (status == CL_BUILD_PROGRAM_FAILURE) {
        size_t log_size;
        clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, 0,
                              NULL, &log_size);
        char *log = (char *) malloc(log_size);
        clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG,
                              log_size, log, NULL);
        fprintf(stderr, "%s\n", log);
        exit(1);
    }
    checkErr(status, "Build program");

    cl_kernel kernel;
    kernel = clCreateKernel(program, "conv", &status);

    // Associate the input and output buffers with the kernel
    status = clSetKernelArg(kernel, 0, sizeof(cl_mem), &buf_Cout);
    checkErr(status, "Set Arg 0");
    status = clSetKernelArg(kernel, 1, sizeof(cl_mem), &buf_Cin);
    checkErr(status, "Set Arg 1");
    status = clSetKernelArg(kernel, 2, sizeof(cl_mem), &buf_weight);
    checkErr(status, "Set Arg 2");
    status = clSetKernelArg(kernel, 3, sizeof(cl_mem), &buf_bias);
    checkErr(status, "Set Arg 3");

    size_t globalWorkSize[3] = {256, 224, 224};
    size_t localWorkSize[3] = {4, 16, 16};

    status = clEnqueueNDRangeKernel(cmdQueue, kernel, 3, NULL,
                                    globalWorkSize, localWorkSize, 0, NULL, NULL);
    checkErr(status, "Execute kernel");

    clEnqueueReadBuffer(cmdQueue, buf_Cout, CL_TRUE, 0,
                        Cout_size, Cout, 0, NULL, NULL);
    checkErr(status, "Dequeue Cout");

}

int main(){
	static float Cout[NUM][OUTIMROW][OUTIMROW];
	static float Cin[NUM][INIMROW][INIMROW];
	static float weight[NUM][NUM][KERNEL][KERNEL];
	static float bias[NUM];

	LoadData(Cin, weight, bias);

	fprintf(stderr, "Start cnn computation\n");
	struct timeval t1, t2;
	gettimeofday(&t1, NULL);

	// --- Please add OpenCL setup code inside the function below ---
   opencl_gpu(
      Cout, Cin, weight, bias
   );

   // --- Timing stuff
	gettimeofday(&t2, NULL);
	float elapsed_time = (t2.tv_sec - t1.tv_sec) + (t2.tv_usec - t1.tv_usec) / 1e6;
	fprintf(stderr, "time(s): %f\n", elapsed_time);
	fprintf(stderr, "GOPs: %f\n", (float)NUM * NUM * IMROW * IMROW * KERNEL * KERNEL * 2 / elapsed_time / 1e9);

   // Please disable the error check before handing in your submission
   // Reminder: We will be measuring your performance externally! (using a unix time invocation)
	int error = Verify(Cout);
	if(error != 0)
		fprintf(stderr, "error ocurrs %d\n", error);
	else
		fprintf(stderr, "all right!\n");

	return 0;
}
