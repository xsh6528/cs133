#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <omp.h>

#define BSIZE 64

// Please modify this function

void mmul(float *A, float *B, float *C, int n) {
    int pnum, pid;
    MPI_Comm_size(MPI_COMM_WORLD, &pnum);
    MPI_Comm_rank(MPI_COMM_WORLD, &pid);

    int n_per_proc = n / pnum;

    // create heap space
    float *local_A = malloc(sizeof(float) * n_per_proc * n);
    if (pid != 0) {
        B = (float *)malloc(sizeof(float) * n * n);
    }
    float *local_C = (float *)calloc(n_per_proc * n, sizeof(float));

    // scatter
    MPI_Bcast(B, n * n, MPI_FLOAT, 0, MPI_COMM_WORLD);
    MPI_Scatter(A, n_per_proc * n, MPI_FLOAT, local_A, n_per_proc * n, MPI_FLOAT, 0, MPI_COMM_WORLD);

    // calc
    int i, j, k, ii, jj, kk;
//#pragma omp parallel for num_threads(2) private(i, j, k, ii, jj)
    for (ii = 0; ii < n_per_proc; ii += BSIZE) {
        for (jj = 0; jj < n; jj += BSIZE) {
            float buf_C[BSIZE][BSIZE] = {0};
            float buf_A[BSIZE][BSIZE];
            float buf_B[BSIZE][BSIZE];

            for (kk = 0; kk < n; kk += BSIZE) {
                for (i = 0; i < BSIZE; i++) {
                    for (j = 0; j < BSIZE; j++) {
                        buf_A[i][j] = local_A[(ii + i) * n + kk + j];
                        buf_B[i][j] = B[(kk + i) * n + jj + j];
                    }
                }

                for (i = 0; i < BSIZE; i++) {
                    for (k = 0; k < BSIZE; k++) {
                        for (j = 0; j < BSIZE; j++) {
                            buf_C[i][j] += buf_A[i][k] * buf_B[k][j];
                        }
                    }
                }
            }
            for (i = 0; i < BSIZE; ++i) {
                memcpy(local_C + (ii + i) * n + jj, buf_C[i], sizeof(float) * BSIZE);
            }
        }
    }

    // gather
    MPI_Gather(local_C, n_per_proc * n, MPI_FLOAT, C, n_per_proc * n, MPI_FLOAT, 0, MPI_COMM_WORLD);

    // free heap space
    free(local_A);
    if (pid != 0) {
        free(B);
    }
    free(local_C);
}

/*
Elapsed Time : 0.047117 secs
Time1 Time : 0.000000 secs
Performance  : 45.58 GFlops
Result Diff  : 0
 */
